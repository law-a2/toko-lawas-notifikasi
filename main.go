package main

import (
	"log"
	"os"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"github.com/streadway/amqp"
	"gitlab.com/law-a2/toko-lawas-notifikasi/database"
	"gitlab.com/law-a2/toko-lawas-notifikasi/handlers"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}

func main() {
	err := godotenv.Load()
	failOnError(err, "Error loading .env file")

	ginMode, exists := os.LookupEnv("GIN_MODE")
	if !exists {
		ginMode = "debug"
	}
	gin.SetMode(ginMode)

	r := gin.Default()

	config := cors.DefaultConfig()
	config.AllowAllOrigins = true
	config.AllowMethods = []string{"POST", "PUT", "PATCH", "DELETE"}
	config.AllowHeaders = []string{"*"}
	config.ExposeHeaders = []string{"*"}
	r.Use(cors.New(config))

	db, err := database.GetClient()
	failOnError(err, "Failed to connect database")

	database.Db = db

	database.InitDb(db)

	notificationAPI := r.Group("")
	{
		notificationAPI.POST("/new", handlers.NewNotification)
		notificationAPI.POST("/get", handlers.GetNotifications)
	}

	rabbitMQUrl := os.Getenv("RABBITMQ_URL")

	conn, err := amqp.Dial(rabbitMQUrl)
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"notifications", // name
		false,           // durable
		false,           // delete when unused
		false,           // exclusive
		false,           // no-wait
		nil,             // arguments
	)
	failOnError(err, "Failed to declare a queue")

	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)
	failOnError(err, "Failed to register a consumer")

	go func() {
		for d := range msgs {
			log.Printf("Received a message from MQ: %s", d.Body)
			handlers.NewNotificationFromMQ(d)
		}
	}()

	port, exists := os.LookupEnv("PORT")
	if !exists {
		port = "8080"
	}
	r.Run(":" + port)

}
