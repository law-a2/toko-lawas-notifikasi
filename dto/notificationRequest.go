package dto

type NewNotificationRequest struct {
	Secret   string `json:"secret"`
	Username string `json:"username"`
	Title    string `json:"title"`
	Content  string `json:"content"`
}

type NofiticationQuery struct {
	Secret   string `json:"secret"`
	Username string `json:"username"`
}
