package dto

type NotificationResponse struct {
	Username      string         `json:"username"`
	Notifications []Notification `json:"notifications"`
}

type Notification struct {
	Title   string `json:"title"`
	Content string `json:"content"`
	IsNew   bool   `json:"is_new"`
}
