package handlers

import (
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"gitlab.com/law-a2/toko-lawas-notifikasi/database"
	"gitlab.com/law-a2/toko-lawas-notifikasi/dto"
)

func GetNotifications(c *gin.Context) {
	err := godotenv.Load()
	failOnError(err, "Error loading .env file")

	var req dto.NofiticationQuery
	if err := c.Bind(&req); err != nil || req.Secret == "" || req.Username == "" {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	secret := os.Getenv("SECRET")
	if req.Secret != secret {
		c.JSON(http.StatusUnauthorized, gin.H{
			"message": "Invalid secret key",
		})
		return
	}

	var notifications []dto.Notification
	database.Db.Table("notifications").Select("title", "content", "is_new").Where("username = ?", req.Username).Scan(&notifications).Updates(map[string]interface{}{"is_new": false})

	response := dto.NotificationResponse{
		Username:      req.Username,
		Notifications: notifications,
	}

	c.JSON(http.StatusOK, response)
}
