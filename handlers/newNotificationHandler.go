package handlers

import (
	"encoding/json"
	"log"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"github.com/streadway/amqp"
	"gitlab.com/law-a2/toko-lawas-notifikasi/database"
	"gitlab.com/law-a2/toko-lawas-notifikasi/dto"
	"gitlab.com/law-a2/toko-lawas-notifikasi/models"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}

func NewNotification(c *gin.Context) {
	err := godotenv.Load()
	failOnError(err, "Error loading .env file")

	var req dto.NewNotificationRequest
	if err := c.Bind(&req); err != nil || req.Secret == "" || req.Username == "" || req.Title == "" || req.Content == "" {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	secret := os.Getenv("SECRET")
	if req.Secret != secret {
		c.JSON(http.StatusUnauthorized, gin.H{
			"message": "Invalid secret key",
		})
		return
	}

	newNotification := models.Notification{
		Username: req.Username,
		Title:    req.Title,
		Content:  req.Content,
		IsNew:    true,
	}

	if dbTrx := database.Db.Create(&newNotification); dbTrx.Error != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": dbTrx.Error})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "notification successfully added to database",
	})
}

func NewNotificationFromMQ(msgs amqp.Delivery) {
	err := godotenv.Load()
	failOnError(err, "Error loading .env file")

	var req dto.NewNotificationRequest
	err = json.Unmarshal(msgs.Body, &req)
	if err != nil {
		log.Println(err)
	}

	if req.Secret == "" || req.Username == "" || req.Title == "" || req.Content == "" {
		log.Println("Received wrong format from MQ")
		return
	}

	secret := os.Getenv("SECRET")
	if req.Secret != secret {
		log.Println("Invalid secret key detected!")
		return
	}

	newNotification := models.Notification{
		Username: req.Username,
		Title:    req.Title,
		Content:  req.Content,
		IsNew:    true,
	}

	if dbTrx := database.Db.Create(&newNotification); dbTrx.Error != nil {
		panic(dbTrx.Error)
	}
}
