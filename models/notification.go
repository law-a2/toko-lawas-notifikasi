package models

type Notification struct {
	Username string `json:"username"`
	Title    string `json:"title"`
	Content  string `json:"content"`
	IsNew    bool   `json:"is_new"`
}
