package database

import (
	"log"
	"os"

	"github.com/joho/godotenv"
	"gitlab.com/law-a2/toko-lawas-notifikasi/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var Db *gorm.DB

func GetClient() (*gorm.DB, error) {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	dsn := os.Getenv("DSN")
	return gorm.Open(postgres.Open(dsn), &gorm.Config{})
}

func InitDb(db *gorm.DB) {
	db.AutoMigrate(&models.Notification{})
}
